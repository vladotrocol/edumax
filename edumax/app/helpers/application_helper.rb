module ApplicationHelper
    def flash_class(level)
        case level
            when 'notice' then "alert alert-dismissable alert-info"
            when 'success' then "alert alert-dismissable alert-success"
            when 'error' then "alert alert-dismissable alert-warning"
            when 'alert' then "alert alert-dismissable alert-warning"
        end
    end
    def admin?
        @current_user.admin == 1
    end
    def get_rank(u)
        if u.admin == 0
            'Student'
        else
            'Teacher'
        end
    end
    def calculate_age(birthday)
        if  birthday
            (Date.today - birthday).to_i / 365
        else
            0
        end
    end
end
