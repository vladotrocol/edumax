json.array!(@news) do |news|
  json.extract! news, :id, :author, :content, :sticky
  json.url news_url(news, format: :json)
end
