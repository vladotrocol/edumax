json.array!(@courses) do |course|
  json.extract! course, :id, :description, :content, :title
  json.url course_url(course, format: :json)
end
