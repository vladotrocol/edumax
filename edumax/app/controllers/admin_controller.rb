class AdminController < ApplicationController
  def basic
    @users=User.all.page(params[:page]).per(2)
  end

  def advanced
    @DB = Hash.new
    get_all_tables(@DB)
  end

  def get_all_tables(myTables)
    ActiveRecord::Base.connection.tables.each do |table|
        next if table.match(/\Aschema_migrations\Z/)
        next if table.match(/\Ackeditor_assets\Z/)
        next if table.match(/\Ataggings\Z/)
        next if table.match(/\Atags\Z/)
        klass = table.singularize.camelize.constantize      
        myTables[klass.name] = klass
    end
  end

  def new_email
    @recipients = User.find(params[:user_ids])
  end
  def send_email
    @recipients = User.find(params[:user_ids])
    @recipients.each do |recipient|
        Notifier.raw_email(recipient.email, params[:user_email][:subject], params[:user_email][:content].html_safe).deliver
    end
    redirect_to(:controller => '/admin', :action => 'basic')
  end

end
