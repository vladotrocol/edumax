class Users::RegistrationsController < Devise::RegistrationsController
  
def edit
  @images = Dir.glob("public/assets/images/users/#{current_user.id}/med/*")
end
  def update
    if params[:user][:image_file_path]      
      ff = File.open("public/"+params[:user][:image_file_path])
      resource.image = ff
      resource.save!
    end
    self.resource = resource_class.to_adapter.get!(send(:"current_#{resource_name}").to_key)
    prev_unconfirmed_email = resource.unconfirmed_email if resource.respond_to?(:unconfirmed_email)

    if params[:user][:password].blank? && params[:user][:password_confirmation].blank?
        params[:user].delete(:password)
        params[:user].delete(:password_confirmation)
        resource_updated = resource.update_without_password(account_update_params)
    else
       resource_updated = resource.update_with_password(account_update_params)
    end

    yield resource if block_given?
    if resource_updated
      if is_flashing_format?
        flash_key = update_needs_confirmation?(resource, prev_unconfirmed_email) ?
          :update_needs_confirmation : :updated
        set_flash_message :notice, flash_key
      end
      sign_in resource_name, resource, bypass: true
      respond_with resource, location: after_update_path_for(resource)
    else
      @images = Dir.glob("public/assets/images/users/#{current_user.id}/med/*")
      clean_up_passwords resource
      respond_with resource
    end
  end


  private
 
  def sign_up_params
    params.require(:user).permit(:first_name, :last_name, :email, :password, :password_confirmation, :telephone, :image, :address, :birthday)
  end
 
  def account_update_params
    params.require(:user).permit(:first_name, :last_name, :email, :password, :password_confirmation, :telephone, :image, :image_file_path, :address, :birthday, :current_password)
  end

  protected 

  def after_update_path_for(resource)
    edit_user_registration_path
  end

end