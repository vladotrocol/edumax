class HomeController < ApplicationController
  def index
    @users = User.all
    @news = News.all.page(params[:page]).per(3).order("created_at DESC")
  end
end
