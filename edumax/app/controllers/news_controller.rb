class NewsController < ApplicationController
  before_action :set_news, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @users = User.all
    @recent = News.all.order("created_at desc").limit(5)
    @news = News.all.page(params[:page]).per(5).order("created_at DESC")

    respond_with(@news)
  end

  def show
    respond_with(@news)
  end

  def new
    @news = News.new
    respond_with(@news)
  end

  def edit
  end

  def create
    @recent = News.all.order("created_at desc").limit(5)
    @news = News.new(news_params)
    @news.author = current_user.id
    if @news.save
      redirect_to news_index_path, notice: "The news has been successfully created."
    else
      flash[:error] = @news.errors.full_messages
      render :new
    end
  end

  def update
    @news.update(news_params)
    respond_with(@news)
  end

  def delete
    @done = News.find(params[:id])
    if @done.destroy
      redirect_to news_index_path, notice: "The news has been successfully deleted."
    else
      redirect_to news_index_path, notice: "The news could not be deleted."
    end
  end

  private
    def set_news
      @news = News.find(params[:id])
    end

    def news_params
      params.require(:news).permit(:author, :content, :sticky)
    end
end
