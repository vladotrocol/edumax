class ArticlesController < ApplicationController
  def index
    @articles = Article.page(params[:page]).per(5).order("created_at DESC")  
    @users = User.all
    @recent = Article.all.order("created_at desc").limit(5)
  end

  def tag
    @articles = Article.tagged_with(params[:tag]).page(params[:page]).per(5).order("created_at DESC")
    @users = User.all
    @recent = Article.all.order("created_at desc").limit(5)
    render :action => 'index'

  end

  def show
    @users = User.all
    @recent = Article.all.order("created_at desc").limit(5)
    @article = Article.find(params[:id])
  end

  def new
    @article = Article.new
    @users = User.all
    @recent = Article.all.order("created_at desc").limit(5)
  end

  def delete
    @done = Article.find(params[:id])
    if @done.destroy
      redirect_to articles_path, notice: "The article has been successfully deleted."
    else
      redirect_to articles_path, notice: "The article could not be deleted."
    end
  end

  def create
    @recent = Article.all.order("created_at desc").limit(5)
    @article = Article.new(article_params)
    @article.author = current_user.id
    if @article.save
      redirect_to articles_path, notice: "The article has been successfully created."
    else
      flash[:error] = @article.errors.full_messages
      render :new
    end
  end

  def edit
    @recent = Article.all.order("created_at desc").limit(5)
    @article = Article.find(params[:id])
  end

  def update
    @recent = Article.all.order("created_at desc").limit(5)
    @article = Article.find(params[:id])
    if @article.update_attributes(article_params)
      redirect_to articles_path, notice: "The article has been successfully updated."
    else
      render action: "edit"
    end
  end



private

  def article_params
    params.require(:article).permit(:title, :body, :author, :tag_list)
  end
end
