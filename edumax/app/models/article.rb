class Article < ActiveRecord::Base
    validates :title, presence: true
    validates :body, presence: true
    validates :author, presence: true
    validates_length_of :title, :minimum => 2, :maximum => 250, :allow_blank => false
    acts_as_taggable
end
