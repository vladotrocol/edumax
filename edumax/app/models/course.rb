class Course < ActiveRecord::Base
    has_attached_file :photo, {styles: { small: "24x24#", smallish: "48x48#", med: "100x100#", large: "200x200#" },
                  :url  => "/assets/images/courses/:style/:basename.:extension",
                  :path => ":rails_root/public/assets/images/courses/:style/:basename.:extension",
                  :default_url => "/assets/images/users/default/:style/man_brown.png",
                  :keep_old_files => true}

    validates_attachment_content_type :photo, :content_type => /\Aimage\/.*\Z/
    validates_attachment_size :photo, :less_than => 2.megabytes
end
