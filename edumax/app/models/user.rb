class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable

    validates :first_name, presence: true
    validates :last_name, presence: true
    validates :telephone, presence: true
    validates_length_of :email, :minimum => 5, :maximum => 250, :allow_blank => false
    
    validates_length_of :first_name, :minimum => 2, :maximum => 250, :allow_blank => false
    validates_length_of :last_name, :minimum => 2, :maximum => 250, :allow_blank => false

    has_attached_file :image, {styles: { small: "24x24#", smallish: "48x48#", med: "100x100#", large: "200x200#" },
                  :url  => "/assets/images/users/:id/:style/:basename.:extension",
                  :path => ":rails_root/public/assets/images/users/:id/:style/:basename.:extension",
                  :default_url => "/assets/images/users/default/:style/man_brown.png",
                  :keep_old_files => true}

    validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
    validates_attachment_size :image, :less_than => 1.megabytes
    devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable
         
end
