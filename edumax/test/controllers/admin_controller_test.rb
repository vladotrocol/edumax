require 'test_helper'

class AdminControllerTest < ActionController::TestCase
  test "should get basic" do
    get :basic
    assert_response :success
  end

  test "should get advanced" do
    get :advanced
    assert_response :success
  end

end
