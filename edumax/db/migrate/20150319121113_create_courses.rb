class CreateCourses < ActiveRecord::Migration
  def change
    create_table :courses do |t|
      t.text :description
      t.text :content
      t.string :title

      t.timestamps
    end
  end
end
