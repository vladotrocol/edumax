class CreateNews < ActiveRecord::Migration
  def change
    create_table :news do |t|
      t.integer :author
      t.string :content
      t.integer :sticky

      t.timestamps
    end
  end
end
