class AddPaperclipToUsers < ActiveRecord::Migration
  def change
    add_attachment :users, :image
    add_column :users, :image_file_path, :string
  end
end
