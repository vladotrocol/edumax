class AddDetailsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :first_name, :string, null: false
    add_column :users, :last_name, :string, null: false
    add_column :users, :telephone, :string, null: false
    add_column :users, :birthday, :date
    add_column :users, :address, :string
    add_column :users, :admin, :integer, null: false, default: 0
  end
end
